package com.ruoyi.order.vo;

/**
 * Created by 魔金商城 on 17/11/16.
 * 退单查询枚举
 */
public enum BackOrderItem {
    LOG, SKUS
}
